Denk dir eine für dich sinnvolle Baumstruktur für deine Compose Files wie auch für deine Daten aus den Containern aus. Hier als Beispiel meine.


# Ordner-Struktur für Compose Dateien (dies liegt bei mir immer unter /root/Projects/docker-repo - meine Konvention):
    docker-repo														<-- Git Repo, zu finden auf https://gitlab.com/hasechris/docker-repo
    ├── mgtsrv001													<-- meine Docker/VM Hosts - hier meine NAS Box im Keller
    │   ├── bacula												<-- Ordner für einen Service
    │   │   ├── docker-compose.yaml				<-- Default Name fürs docker-compose File
    │   │   └── sd												<-- optionale weitere Sachen für den Service
    │   │       ├── Dockerfile
    │   │       └── run
    │   ├── dhcp-seniorenwg
    │   │   └── docker-compose.yml
    │   ├── dns-hasiatthegrill
    │   │   └── docker-compose.yml
    │   ├── dns-hasiatthegrill-seniorenwg
    │   │   └── docker-compose.yml
    │   ├── dns-proxy-extern
    │   │   ├── dnsmasq.conf
    │   │   └── docker-compose.yml
    │   ├── dns-seniorenwg-bind
    │   │   └── docker-compose.yml
    │   ├── freeradius
    │   │   └── docker-compose.yml
    │   ├── gitlab-runner-mgtsrv001
    │   │   └── docker-compose.yml
    │   ├── heimdall
    │   │   └── docker-compose.yml
    │   ├── iobroker
    │   │   └── docker-compose.yml
    │   ├── laplace
    │   │   └── docker-compose.yml
    │   ├── librenms
    │   │   ├── docker-compose.yml
    │   │   └── librenms.env
    │   ├── mopidy
    │   ├── network
    │   │   └── docker-compose.yml
    │   ├── nextcloud
    │   │   ├── docker-compose.secrets.yml
    │   │   ├── docker-compose.secrets.yml.secret
    │   │   └── docker-compose.yml
    │   ├── phpipam
    │   │   ├── docker-compose.secrets.yml
    │   │   ├── docker-compose.secrets.yml.secret
    │   │   └── docker-compose.yml
    │   ├── pihole
    │   │   └── docker-compose.yml
    │   ├── py-kms
    │   │   └── docker-compose.yml
    │   ├── sql
    │   │   ├── docker-compose.secrets.yml
    │   │   ├── docker-compose.secrets.yml.secret
    │   │   └── docker-compose.yml
    │   ├── teamspeak
    │   │   └── docker-compose.yml
    │   ├── traefik
    │   │   └── docker-compose.yml
    │   ├── unifi
    │   │   └── docker-compose.yml
    │   └── watchtower
    │       └── docker-compose.yml
    └── mgtsrv002													<-- nächster Docker Host - hier mein VServer im Internet bei PHPfriends
        ├── minecraft-designworld
        │   └── docker-compose.yml
        ├── teamspeak
        │   └── docker-compose.yml
        ├── valheim
        │   └── docker-compose.yml
        └── watchtower
            └── docker-compose.yml

# Ordner-Struktur für Daten der Container auf meiner NAS Box (mgtsrv001):
    /vm_satassd1                            <-- Mountpoint meiner SATA-SSD
    ├── docker                              <-- Haupt-Ordner für Daten meiner Container
    │   ├── freeipa
    │   ├── images
    │   ├── nextcloud-sql                   <-- Ordner für Live-Daten des SQL Containers meiner Nextcoud (siehe Compose File unter docker-repo/mgtsrv001/nextcloud/docker-compose.yml den Service nextcloud-sql)
    │   ├── phpipam-sql
    │   ├── sql
    │   ├── tinc_retiolum
    │   └── unifi
    ├── lxc                                 <-- Ordner für Daten meiner LXC Container (ist erledigt und wird abgebaut)
    │   ├── subvol-1002-disk-0
    │   └── templates
    ├── tape                                <-- Zwischenspeicher für Backup-Daten, die auf den LTO Taperoboter geschrieben werden sollen (Projekt noch im Aufbau)
    │   └── buffer
    └── vms
        └── isos														<-- schneller Lese-Ort für ISO Images (hilft bei VM Installationen natürlich sehr)
    
    /data_hdd                               <-- Mountpoint des großen RAID Verbundes aus 8x 2TB HDDs
    ├── backup                              <-- Ordner für Backups -- hier kommen Backups vom meinem vServer bei phpfriends über SSH an und meine SQL Docker Container schreiben SQL-Dumps hier hin (nextcloud und phpipam), da die Live-Daten der MySQL DBs nicht crash-konsistent sind
    │   ├── backupvalheimsteffen
    │   ├── backupvalheimsteffen_backup
    │   ├── nextcloud
    │   └── phpipam
    ├── docker                              <-- Hier werden Daten von Containern gespeichert, die crash-consistent sind, daher direkt auf das große RAID. Beispiel hierbei sind Config-Dateien von Services oder die eigentlichen Daten, die ich in meiner Nextcloud hochlade.
    |   ├── bacula
    |   │   ├── baculum
    |   │   ├── psql
    |   │   └── shared-configs
    |   ├── baculum
    |   ├── dhcp-seniorenwg
    |   │   └── config
    |   ├── dns-hasiatthegrill
    |   │   ├── dbs
    |   │   └── named.conf
    |   ├── dns-hasiatthegrill-seniorenwg
    |   │   ├── dbs
    |   │   └── named.conf
    |   ├── dns-seniorenwg
    |   ├── gitlab-runner-mgtsrv001
    |   │   ├── config.toml
    |   │   └── config.toml.lock
    |   ├── heimdall
    |   │   └── config
    |   ├── iobroker
    |   │   ├── opt
    |   │   └── tmp
    |   ├── librenms
    |   │   ├── librenms
    |   │   ├── mysql
    |   │   └── rrd-journal
    |   ├── nextcloud
    |   │   ├── collabora
    |   │   ├── le-certs
    |   │   ├── nextcloud
    |   │   ├── nextcloud_elasticsearch
    |   │   ├── nginx-html
    |   │   ├── nginx-vhost
    |   │   ├── nginx-vhosts
    |   │   ├── php_ini
    |   │   ├── shared
    |   │   ├── sql_alt
    |   │   ├── sql_lxc
    |   │   └── sql_test_import
    |   ├── pihole
    |   │   ├── etc-dnsmasq.d
    |   │   └── etc-pihole
    |   ├── traefik
    |   │   └── traefik
    |   ├── ts3
    |   │   ├── crashdumps
    |   │   ├── files
    |   │   ├── logs
    |   │   ├── query_ip_blacklist.txt
    |   │   ├── query_ip_whitelist.txt
    |   │   ├── ssh_host_rsa_key
    |   │   ├── ts3server.sqlitedb
    |   │   ├── ts3server.sqlitedb-shm
    |   │   └── ts3server.sqlitedb-wal
    |   └── ts3.tar
    ├── minecraft_designworld_backup
    │   ├── Backup-2020-10-13-22-26-26.zip
    │   ├── backups
    │   └── test2
    ├── nfs
    │   └── gitlab-runner-desktop
    └── vms
    
# Docker Befehle
docker run						--> erstelle einen neuen Container und starte das im Dockerfile hinterlegte COMMAND
docker exec						--> führe ein Command in einem gestarteten Container aus
docker run/exec -it		--> startet einen Container interaktiv. Bedeutet, dass die Shell-Eingabe als STDIN an den Container und STDOUT vom Container an das STDOUT deiner Shell angehängt wird
docker run -d					--> starte einen Container im Hintergrund (--detached), dies ist der eigentlich normale Modus für Container
docker stop						--> stoppe einen Container
docker kill						--> kille einen Container (wenn er hängt und sich nicht beenden lässt)
docker rm							--> lösche einen Container

# docker-compose (benötigt die Datei docker-compose.yml im aktuellen Verzeichnis oder den Parameter -f "datei")
docker-compose up		  										--> lege Container und virtuelle Netze für alle Services an und starte alle Services interaktiv. Die STDOUT der Container wird an deine Shell weitergeleitet.
  																							eigene Befehle in eine "Shell" eines Container eingeben kannst du hier nicht.
docker-compose up -d											--> lege Container und virtuelle Netze für alle Services an und starte alle Services im Hintergrund (wie bei Docker oben sollte dies der normale Anwendungsfall sein)
docker-compose down												--> stoppe alle Services und lösche die Container und Netzwerke
docker-compose up/up -d "servicename"			--> lege Container und virtuelle Netze für den angegebenen Service an
docker-compose -f composefile-blub.yml		--> gebe eine andere Datei für Docker-compose an. Es können mehrere Dateien angeben, die Yaml Bäume werden dann verschmolzen. Bei gleichen Bäumen überschreibt die zweite
																							Datei Daten aus der ersten Datei. zu dem Yaml Thema später mehr.
                                              nach dem -f "Datei" gibts du einfach die normalen Compose Befehle wie "up -d" oder "down" an


# Docker container laufen lassen - Image ohne "Applikation" (das Command ist hierbei eine Bash, die im Hintergrund ausgeführt wird. Die beendet sich sofort wieder)
	docker run -d debian:latest
  	11fbab76461153da02b6125ac222e16915c5e6fce7e93d6bc841420f9eb51b3d

	docker ps -a                                                                                           
    CONTAINER ID   IMAGE                                                                 COMMAND                   CREATED         STATUS                     PORTS     NAMES
    39574bc8a688   debian:latest                                                         "bash"                    4 seconds ago   Exited (0) 3 seconds ago             great_kowalevski

# docker container laufen lassen mit "richtiger" Anwendung (hierbei wird ein Webserver mit einer kleinen statischen Seite gestartet)
	docker run -d ghcr.io/linuxserver/heimdall                                                                                                                                                                           
		1bf0d1c81a99002a10f66fe40dc734a44818f6f1bef22501eb580838c29c0a2b

	docker ps -a                                                                                                                                                                                                         
		CONTAINER ID   IMAGE                                                                 COMMAND                   CREATED          STATUS                      PORTS             NAMES
		1bf0d1c81a99   ghcr.io/linuxserver/heimdall                                          "/init"                   4 seconds ago    Up 2 seconds                80/tcp, 443/tcp   flamboyant_hodgkin

Dieser Container benutzt die Ports 80 + 443, man kommt aber noch nicht an sie ran von außen. Auf dem Host, wo der Container läuft, könnte man die IP des Containers angeben.
	
	docker container inspect flamboyant_hodgkin
	  [
  	  {
      ...
          "NetworkSettings": {
					...
          	"IPAddress": "172.17.0.2",
          ...
          }
      }
    ]

Dann kannst du die IP im Browser mit den oben angezeigten Ports öffnen. Ist aber blöd, da die IP sich bei jedem Container restart oder Neu-Anlegen ändert. Daher kommen wir erstmal nun zum Docker Netzwerk

# Docker Network
## Default Docker Netzwerke (sind immer vorhanden)

	docker network ls
  	NETWORK ID     NAME                         DRIVER    SCOPE
		c548bad369a7   bridge                       bridge    local
		2437392b5d83   host                         host      local
		6d25447b5ad3   none                         null      local

	- Bridge: Hierbei wird auf deiner Host-IP ein Proxy-Dienst auf den definierten Ports gestartet. Dieser leitet Traffic an den Container weiter. Hier ein "netstat -tunpl" auf dem Host.

			docker ps -a
				CONTAINER ID   IMAGE                                                                 COMMAND                   CREATED         STATUS                     PORTS                         NAMES
				563c1c8d6bfd   ghcr.io/linuxserver/heimdall                                          "/init"                   3 seconds ago   Up 2 seconds               0.0.0.0:80->80/tcp, 443/tcp   romantic_ptolemy

			sudo netstat -tunpl
				Active Internet connections (only servers)
				Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
				tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      291478/docker-proxy 

  - Host: Macht fast das gleiche wie Bridge, der Container bekommt nur kein eigenes internes Netz mit einer eigenen IP, sondern wird direkt auf deine Host-IP gebunden.
	- None: Der Container hat kein Netzwerk. Es existiert nur eine loopback (lo unter Linux) Netzwerkkarte im Container.
  
## Wahl des richtigen Docker-Netzwerk-Typs
Das ist wiederum relativ einfach. Meine beiden Docker-Hosts als Beispiel:
- mgtsrv001: Meine NAS Box mit sehr vielen Services. Dieser hat diese Docker Netzwerke:

		root@mgtsrv001:~# docker network ls
			NETWORK ID          NAME                      DRIVER              SCOPE
			79a059a13d91        bridge                    bridge              local
			35f23925d29a        docker-internal           bridge              local
			ba9e40ae2201        docker-vmbr0              ipvlan              local
			f00e1df99d09        docker-vmbr2              ipvlan              local
			ea6fcfeb94e5        docker-vmbr10             ipvlan              local
			c0054e3fc2df        docker-vmbr14             ipvlan              local
			9f89e4c235b1        docker-vmbr210            ipvlan              local
			38fb09d13a4f        host                      host                local
			b27c99121aaa        nextcloud_default         bridge              local
			73233c3d27c4        nextcloud_nextcloud-sql   bridge              local
			5903914daffe        none                      null                local
			48c1348476d4        proxy-withinternet        bridge              local
			5d59e577ae89        watchtower_default        bridge              local

- mgtsrv002: Mein vServer bei PHPfriends -- Services, die im Internet für alle erreichbar sind. Hier die Docker Netzwerke:

		root@mgtsrv002:~ # docker network ls
			NETWORK ID          NAME                            DRIVER              SCOPE
			defe63bcd05a        bridge                          bridge              local
			d9c86dd51ff9        host                            host                local
			bb3ca9f5d84d        minecraft-designworld_default   bridge              local
			20988ab4de91        none                            null                local
			059aec4c8eeb        teamspeak_default               bridge              local
			77c06ccc3e01        valheim_default                 bridge              local

Auf meiner NAS Box habe ich Netze pro VLAN (von außen kommend). Unser Netzwerk hier daheim ist aufgeteilt in verschiedene Bereiche. Netze für die Geräte der Mieter, ein Shared-Netz für zentrale Services und Server sowie meine DMZ für Dienste, die aus dem Internet erreichbar sind (Nextcloud oder der DNS Server für hasiatthegrill.net). Daher habe ich pro VLAN eine Docker Bridge, sodass ich Containern IPs in dem Subnetz geben kann.

Auf meinem vServer habe ich nur eine Hand voll Dienste laufen. Da ich hierbei quasi niemals mehrere Dienste mit gleichen Ports haben werde habe ich nur ein Subnetz von außen an dem vServer dran und die Docker-Container hängen über das servicename_default Netzwerk direkt an der Host-IP dran. Das ist wie das Default "bridge" Netzwerk. Meine Firewall macht dann ein Port-Forwarding von außen an die richtigen Ports auf dem vServer.


# Docker-Compose
## Syntax Compose Datei
Die Dateien sind in YAML geschrieben. Das sind Datenbäume, die durch zwei Leerzeichen die Hierarchie definiert bekommen, also pro Ebene einfach die entsprechende Menge an 2x Leerzeichen an den Anfang der Zeile. Kommentare wie gewohnt mit "#".
###### Beginn YAML
version: '2.4'
networks:
  docker-vmbr210:
    external: true
  docker-internal:
    external: true
  proxy-withinternet:
    external: true

services:
  traefik:															<-- name eines Services (container)
    image: traefik:latest								<-- image URL (bei den Kurznamen wie hier kommts vom Docker Hub)				
    restart: always											<-- wann soll der Container neu gestartet werden
    command: --providers.docker					<-- wenn im Dockerfile ein "entrypoint" definiert ist werden über command zusätzliche Parameter angegeben. Ansonsten direkt das auszuführende Command im Container
    #ports:															<-- hier würden Ports über einen Docker-Proxy von der Host-IP weitergeleitet werden
    #  - "80:80"
    #  - "443:443"
      #- "8080:8080"
    volumes:														<-- Volumes. Dazu unten mehr.
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /data_hdd/docker/traefik/traefik/traefik.toml:/traefik.toml
      - /data_hdd/docker/traefik/traefik/non_docker_servers.toml:/non_docker_servers.toml
      - /data_hdd/docker/traefik/traefik/acme.json:/acme.json
    networks:														<-- Networks: auch dazu unten mehr
      docker-vmbr210:
        ipv4_address: 10.253.42.30
      docker-internal:
        aliases:
          - traefik
      proxy-withinternet:
        aliases:
          - traefik
    labels:															<-- Labels sind einfache Eigenschaften, die dem Container angehängt werden. Diese siehst du auch im Befehl "docker container inspect -name-".
      - "traefik.enable=false"
###### Ende YAML

## Volumes in Compose
Es gibt zwei Wege. Ein Docker Volume oder ein Bind-Mount. Ich benutze Docker Volumes nie, da docker keinen nutzbares Management der Docker Volumes eingebaut hat. Also Aktionen wie Exportieren, als Zip packen, zu einem anderen Host kopieren etc. Ich benutze daher immer Bind-Mounts.

#### volume
volumes:
  - volumename:/pfad/im/container
  
### Bind-Mounts
volumes:
  - /pfad/im/Host:/pfad/im/Container
  
### Read-Only Bind-Mounts/Volumes
volumes:
  - volumename:/pfad/im/container:ro			<-- das ":ro" am Ende der Zeile
  - /pfad/im/Host:/pfad/im/Container:ro

## Netzwerk
### Forwarding mit "Bridge" - wie auf meinem vServer
version: '3.4'
services:
  ts3:
    image: teamspeak
    container_name: ts3
    restart: always
    command: ts3server serveradmin_password=#snipped#
    ports:																						<-- relevanter Teil
      - 9987:9987/udp					<-- port außen:port im Container (plus /udp falls es udp Traffic ist)
      - 10011:10011
      - 30033:30033

### Linux Bridges mit externen IPs am Container
version: '3.4'
networks:
  docker-vmbr210:
    external: true						<-- dies definiert, dass docker-compose dieses Netzwerk nicht bei den Aktionen "up" oder "down" anlegt oder löscht
services:
  ts3:
    image: teamspeak
    restart: always
    environment:
      #TS3SERVER_DB_PLUGIN: ts3db_mariadb
      #TS3SERVER_DB_SQLCREATEPATH: create_mariadb
      #TS3SERVER_DB_HOST: ts3_db
      #TS3SERVER_DB_USER: root
      #TS3SERVER_DB_PASSWORD: example
      #TS3SERVER_DB_NAME: teamspeak
      #TS3SERVER_DB_WAITUNTILREADY: 30
      TS3SERVER_LICENSE: accept
    networks:														<-- relevanter Teil
      docker-vmbr210:										<-- Zuweisung des definierten Netzes zu dem Container
        ipv4_address: 10.253.42.11			<-- Zuweisung einer IP aus dem externen Netz
        
### Anlegen der Linux Bridges (Zugang zu VLANs/Subnetzen von außen)
Diese Netze werden einmal im Docker angelegt und können bei beliebig vielen Containern benutzt werden. Hier ein Auszug aus der Datei "docker-repo/mgtsrv001/network/docker-compose.yml".

version: '2.4'							<-- wichtig, bei der Networks-Datei immer Version 2.4 angeben. die neuere Compose Version 3 kann damit nicht sauber umgehen (Stand April 2020).
networks:
  ...
  docker-vmbr0:							<-- VLAN 0 (Null gibts ja nicht, das ist eine Brücke direkt auf vmbr0 was eth0 auf dem Docker Host entspricht)
    name: docker-vmbr0
    driver: ipvlan
    driver_opts:
      ipvlan_mode: l2				<-- Wichtig: Layer2 Brücke, sodass die Container direkt auf MAC-Ebene in das entsprechende Netz Zugriff haben.
      parent: vmbr0					<-- Die Netzwerkkarte auf dem Docker-Host
    ipam:										<-- Hier wird definiert, welche Subnetze verfügbar sind
      config:
        - subnet: 192.168.42.0/27
        - subnet: 192.168.1.0/24
	...
  docker-vmbr10:
    name: docker-vmbr10
    driver: ipvlan
    driver_opts:
      ipvlan_mode: l2
      parent: vmbr10
    ipam:
      config:
        - subnet: 10.10.10.254/24
          gateway: 10.10.10.254					<-- Zusatz: Falls das Gateway nicht auf der ersten IP des Subnetzes verfügbar ist
          ip_range: 10.10.10.248/29			<-- Zusatz: Man kann hier die Range der nutzbaren IPs einschränken, die einem Container zugewiesen werden darf. Abgestimmt mit dem DHCP Server eine gute Idee ;)

  docker-internal:											<-- Ein internes Netzwerk (aufgrund des Parameters internal:true)
    name: docker-internal
    internal: true
	...
services:
  dummy:																<-- Ein Dummy Container. Entrypoint wird überschrieben mit einem Echo. Es werden aber alle Networks aus dem "networks" Segment angegeben. Daher werden diese Netzwerke alle angelegt.
    container_name: network-dep-dummy
    image: williamyeh/dummy
    entrypoint: ["echo", "Networks generated"]
    networks:
      - docker-vmbr14
      - docker-vmbr0
      - docker-vmbr2
      - docker-vmbr10
      - docker-internal
      - proxy-withinternet
      - docker-vmbr210
    restart: "no"


# Passwörter in Compose-Files
kommt später ;)

# Reverse-Proxy über Traefik (extra auf Docker-Betrieb angepasster Reverse-Proxy)
kommt später ;)
